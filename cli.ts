#!/usr/bin/env node

/// <reference path='_all.d.ts' />

'use strict';

import fs = require('fs');
import http = require('http');
import commander = require('commander');
import { Sitemap } from './lib/sitemap';
import * as utils from './lib/utils';
import { Stats } from "fs";

const DEFAULT_LOCALES = [
  {url: "ae", hreflang: "en-ae"},
  {url: "au", hreflang: "en-au"},
  {url: "ca", hreflang: "en-ca"},
  {url: "ie", hreflang: "en-ie"},
  {url: "gb", hreflang: "en-gb"},
  {url: "nz", hreflang: "en-nz"},
  {url: "us", hreflang: "en"}
];

commander.version('0.0.1')
  .usage('[options]')
  .option('--url <url>', 'site url address')
  .option('--pages <pages>', 'path to pages.json, default "./pages.json"', './pages.json')
  .option('--save <save>', 'path to save sitemap.xml, default "./"', './')
  .option('--global <global>', 'global locale, default "us"', 'us')
  .action(() => {

  })
  .parse(process.argv);

if (!commander['url']) {
  commander.help();
}

fs.stat(commander['pages'], (err: NodeJS.ErrnoException, stats: Stats) => {
  if (!stats.isFile()) {
    console.error(`Pages JSON file not found on path: ${commander['pages']}`);
    // commander.exit(1);
    return 0;
  } else {
    fs.readFile(commander['pages'], 'utf8', (err: NodeJS.ErrnoException, data: string) => {
      let sitemap = new Sitemap(commander['url'], commander['save'], commander['global'], DEFAULT_LOCALES);
      let confPages = JSON.parse(data);

      let pages = sitemap.process(confPages);

      // TODO Вынести в callback
      http.get('http://app.essaypro.com/api/catalog?limit=50', (res: http.IncomingMessage) => {
        res.setEncoding('utf8');
        let responseData = '';

        res.on('data', (chunk) => {
          responseData += chunk;
        });
        res.on('end', () => {
          let jsonResponse = JSON.parse(responseData);
          let writers: IWriter[] = jsonResponse['writers'];
          writers.forEach((writer: IWriter) => {
            let page = {
              url: `essay-writer.html?id=${writer.id}`,
              locales: DEFAULT_LOCALES
            };
            pages.push(page);
          });
          let xml = sitemap.generate(pages);
          sitemap.save(xml, false);
          // TODO Generate siteindex.xml
          // commander.exit(0);
        })
      }).on('error', (e) => {
        console.log(`Got error: ${e.message}`);
      });
    });
  }
});

export interface IWriter {
  id: number;
  fullname: string;
  nickname: string;
  online: boolean;
  onlineDate: any;
  description: string;
  subjectArea: any;
  shortDescription: string;
  avatarSrc: string;
  stats: any;
}
