"use strict";
const fs = require('fs');
const utils = require('./utils');
class Sitemap {
    // private sitemaps: ISitemap[] = [];
    constructor(siteURL, savePath = './', defaultLocaleUrl = 'en', defaultLocales) {
        this.savePath = savePath;
        this.defaultLocaleUrl = defaultLocaleUrl;
        this.defaultLocales = defaultLocales;
        this.siteURL = siteURL;
        if (!/^(f|ht)tps?:\/\//i.test(siteURL)) {
            this.siteURL = `http://${this.siteURL}`;
        }
    }
    process(confPages) {
        let pages = [];
        Object.keys(confPages).forEach((confPageKey) => {
            let confPage = confPages[confPageKey];
            if (!confPage['locales']) {
                confPage.locales = this.defaultLocales;
            }
            pages.push({
                url: ((confPage['url'] !== null && confPage['url'] !== undefined) ? confPage['url'] : confPage.filename),
                locales: confPage.locales
            });
        });
        return pages;
    }
    generate(pages) {
        let xml = `<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">`;
        pages.forEach((page) => {
            page.locales.forEach((locale) => {
                let url = page.url;
                if (locale.url !== this.defaultLocaleUrl) {
                    url = `${locale.url}/${url}`;
                }
                xml += `<url><loc>${this.siteURL}/${url}</loc>`;
                page.locales.forEach((currLocale) => {
                    let currUrl = `${page.url}`;
                    if (currLocale.url !== this.defaultLocaleUrl) {
                        currUrl = `${currLocale.url}/${currUrl}`;
                    }
                    xml += `<xhtml:link rel="alternate" hreflang="${currLocale.hreflang}" href="${this.siteURL}/${currUrl}" />`;
                });
                xml += `</url>`;
            });
        });
        xml += '</urlset>';
        return xml;
    }
    save(xml, minify = true) {
        // let xml: string = this.generate(pages);
        let filename = '/sitemap';
        // if (this.sitemaps.length > 0) {
        //   filename += `-${this.sitemaps.length}`;
        // }
        filename += '.xml';
        if (minify) {
            xml = utils.minXml(xml);
        }
        else {
            xml = utils.prettyXml(xml);
        }
        fs.writeFileSync(this.savePath + filename, xml);
        let sitemap = {
            loc: filename,
            lastMod: new Date()
        };
        return sitemap;
        // this.sitemaps.push(sitemap);
        // TODO Build siteindex
    }
}
exports.Sitemap = Sitemap;
