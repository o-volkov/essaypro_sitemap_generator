export function toW3CString(date: Date) {
  let year: number = date.getFullYear();
  let yearString: string = year.toString();
  let month: number = date.getMonth();
  let monthString: string = month.toString();
  month++;
  if (month < 10) {
    monthString = '0' + month.toString();
  }

  let day: number = date.getDate();
  let dayString: string = day.toString();
  if (day < 10) {
    dayString = '0' + day.toString();
  }

  let hours: number = date.getHours();
  let hoursString: string = hours.toString();
  if (hours < 10) {
    hoursString = '0' + hours.toString();
  }

  let minutes: number = date.getMinutes();
  let minutesString: string = minutes.toString();
  if (minutes < 10) {
    minutesString = '0' + minutes.toString();
  }

  let seconds: number = date.getSeconds();
  let secondsString: string = seconds.toString();
  if (seconds < 10) {
    secondsString = '0' + seconds.toString();
  }

  let offset: number = -date.getTimezoneOffset();
  let offsetHours: number = Math.abs(Math.floor(offset / 60));
  let offsetHoursString: string = offsetHours.toString();
  if (offsetHours < 10) {
    offsetHoursString = '0' + offsetHours.toString();
  }

  let offsetMinutes: number = Math.abs(offset) - offsetHours * 60;
  let offsetMinutesString: string = offsetMinutes.toString();
  if (offsetMinutes < 10) {
    offsetMinutesString = '0' + offsetMinutes.toString();
  }

  let offsetSign: string = '+';
  if (offset < 0) {
    offsetSign = '-';
  }

  return `${yearString}-${monthString}-${dayString}T${hoursString}:${minutesString}:${secondsString}${offsetSign}${offsetHoursString}:${offsetMinutesString}`;
  // return yearString + '-' + monthString + '-' + dayString +
  //   'T' + hoursString + ':' + minutesString + ':' + secondsString +
  //   offsetSign + offsetHoursString + ':' + offsetMinutesString;
}

export function prettyXml(text) {
  let shift = ['\n'];
  let step = '  ';
  let maxdeep = 1000;

  // initialize array with shifts //
  for (let ix=0; ix < maxdeep; ix++) {
    shift.push(shift[ix] + step);
  }

  let ar = text.replace(/>\s{0,}</g, "><")
      .replace(/</g, "~::~<")
      .replace(/xmlns\:/g, "~::~xmlns:")
      .replace(/xmlns\=/g, "~::~xmlns=")
      .split('~::~'),
    len = ar.length,
    inComment = false,
    deep = 0,
    str = '';

  for (let ix = 0; ix < len; ix++) {
    // start comment or <![CDATA[...]]> or <!DOCTYPE //
    if (ar[ix].search(/<!/) > -1) {
      str += shift[deep] + ar[ix];
      inComment = true;
      // end comment  or <![CDATA[...]]> //
      if (ar[ix].search(/-->/) > -1 || ar[ix].search(/\]>/) > -1 || ar[ix].search(/!DOCTYPE/) > -1) {
        inComment = false;
      }
    } else {
      // end comment  or <![CDATA[...]]> //
      if (ar[ix].search(/-->/) > -1 || ar[ix].search(/\]>/) > -1) {
        str += ar[ix];
        inComment = false;
      } else {
        // <elm></elm> //
        if (/^<\w/.exec(ar[ix - 1]) && /^<\/\w/.exec(ar[ix]) &&
          /^<[\w:\-\.\,]+/.exec(ar[ix - 1]).toString() == /^<\/[\w:\-\.\,]+/.exec(ar[ix])[0].replace('/', '').toString()) {
          str += ar[ix];
          if (!inComment) {
            deep--;
          }
        } else {
          // <elm> //
          if (ar[ix].search(/<\w/) > -1 && ar[ix].search(/<\//) == -1 && ar[ix].search(/\/>/) == -1) {
            str = !inComment ? str += shift[deep++] + ar[ix] : str += ar[ix];
          } else {
            // <elm>...</elm> //
            if (ar[ix].search(/<\w/) > -1 && ar[ix].search(/<\//) > -1) {
              str = !inComment ? str += shift[deep] + ar[ix] : str += ar[ix];
            } else {
              // </elm> //
              if (ar[ix].search(/<\//) > -1) {
                str = !inComment ? str += shift[--deep] + ar[ix] : str += ar[ix];
              } else {
                // <elm/> //
                if (ar[ix].search(/\/>/) > -1) {
                  str = !inComment ? str += shift[deep] + ar[ix] : str += ar[ix];
                } else {
                  // <? xml ... ?> //
                  if (ar[ix].search(/<\?/) > -1) {
                    str += shift[deep] + ar[ix];
                  } else {
                    // xmlns //
                    if (ar[ix].search(/xmlns\:/) > -1 || ar[ix].search(/xmlns\=/) > -1) {
                      str += shift[deep] + ar[ix];
                    } else {
                      str += ar[ix];
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  return (str[0] == '\n') ? str.slice(1) : str;
}

export function minXml(text) {
  let str = text.replace(/\<![ \r\n\t]*(--([^\-]|[\r\n]|-[^\-])*--[ \r\n\t]*)\>/g,"");
  return str.replace(/>\s{0,}</g,"><");
}
