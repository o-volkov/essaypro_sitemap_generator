import fs = require('fs');
import {ISitemap} from './sitemap';
import * as utils from './utils';

export class SiteIndex {
  private sitemaps: ISitemap[] = [];

  constructor(sitemaps: ISitemap[]) {
    this.sitemaps = sitemaps;
  }

  generate(): string {
    let xml = `
<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd"
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    `;

    this.sitemaps.forEach((sitemap: ISitemap) => {
      xml += `
        <sitemap>
          <loc>${sitemap.loc}</loc>
          <lastmod>${utils.toW3CString(sitemap.lastMod)}</lastmod>
        </sitemap>
      `;
    });

    xml += '</sitemapindex>';

    return xml;
  }

  save(xml: string, savePath: string): void {
    fs.writeFileSync(`${savePath}/siteindex.xml`, xml);
  }
}
