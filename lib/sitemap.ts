import fs = require('fs');
import * as utils from './utils';

export class Sitemap {
  private siteURL: string;
  // private sitemaps: ISitemap[] = [];

  constructor(siteURL, private savePath: string = './', private defaultLocaleUrl: string = 'en', private defaultLocales?: {url: string, hreflang: string}[]) {
    this.siteURL = siteURL;

    if (!/^(f|ht)tps?:\/\//i.test(siteURL)) {
      this.siteURL = `http://${this.siteURL}`;
    }
  }

  process(confPages: any): IPage[] {
    let pages: IPage[] = [];
    Object.keys(confPages).forEach((confPageKey: string) => {
      let confPage = confPages[confPageKey];

      if (!confPage['locales']) {
        confPage.locales = this.defaultLocales;
      }

      pages.push({
        url: ((confPage['url'] !== null && confPage['url'] !== undefined) ? confPage['url'] : confPage.filename),
        locales: confPage.locales
      })
    });

    return pages;
  }

  generate(pages: IPage[]): string {
    let xml = `<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">`;

    pages.forEach((page: IPage) => {
      page.locales.forEach((locale: ILocale) => {
        let url = page.url;
        if (locale.url !== this.defaultLocaleUrl) {
          url = `${locale.url}/${url}`
        }

        xml += `<url><loc>${this.siteURL}/${url}</loc>`;

        page.locales.forEach((currLocale: ILocale) => {
          let currUrl = `${page.url}`;
          if (currLocale.url !== this.defaultLocaleUrl) {
            currUrl = `${currLocale.url}/${currUrl}`
          }

          xml += `<xhtml:link rel="alternate" hreflang="${currLocale.hreflang}" href="${this.siteURL}/${currUrl}" />`;
        });

        xml += `</url>`;
      });
    });

    xml += '</urlset>';

    return xml;
  }

  save(xml: string, minify: boolean = true) {
    // let xml: string = this.generate(pages);

    let filename = '/sitemap';

    // if (this.sitemaps.length > 0) {
    //   filename += `-${this.sitemaps.length}`;
    // }

    filename += '.xml';

    if (minify) {
      xml = utils.minXml(xml);
    } else {
      xml = utils.prettyXml(xml);
    }

    fs.writeFileSync(this.savePath + filename, xml);

    let sitemap: ISitemap = {
      loc: filename,
      lastMod: new Date()
    };

    return sitemap;

    // this.sitemaps.push(sitemap);
    // TODO Build siteindex
  }
}


export interface ILocale {
  url: string;
  hreflang: string;
}

export interface IPage {
  url: string,
  locales?: ILocale[]
}

export interface ISitemap {
  loc: string;
  lastMod: Date;
}
