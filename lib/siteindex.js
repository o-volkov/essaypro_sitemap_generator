"use strict";
const fs = require('fs');
const utils = require('./utils');
class SiteIndex {
    constructor(sitemaps) {
        this.sitemaps = [];
        this.sitemaps = sitemaps;
    }
    generate() {
        let xml = `
<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd"
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    `;
        this.sitemaps.forEach((sitemap) => {
            xml += `
        <sitemap>
          <loc>${sitemap.loc}</loc>
          <lastmod>${utils.toW3CString(sitemap.lastMod)}</lastmod>
        </sitemap>
      `;
        });
        xml += '</sitemapindex>';
        return xml;
    }
    save(xml, savePath) {
        fs.writeFileSync(`${savePath}/siteindex.xml`, xml);
    }
}
exports.SiteIndex = SiteIndex;
