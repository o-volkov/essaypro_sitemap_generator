#!/usr/bin/env node
'use strict';
const fs = require('fs');
const http = require('http');
const commander = require('commander');
const sitemap_1 = require('./lib/sitemap');
const DEFAULT_LOCALES = [
    { url: "ae", hreflang: "en-ae" },
    { url: "au", hreflang: "en-au" },
    { url: "ca", hreflang: "en-ca" },
    { url: "ie", hreflang: "en-ie" },
    { url: "gb", hreflang: "en-gb" },
    { url: "nz", hreflang: "en-nz" },
    { url: "us", hreflang: "en" }
];
commander.version('0.0.1')
    .usage('[options]')
    .option('--url <url>', 'site url address')
    .option('--pages <pages>', 'path to pages.json, default "./pages.json"', './pages.json')
    .option('--save <save>', 'path to save sitemap.xml, default "./"', './')
    .option('--global <global>', 'global locale, default "us"', 'us')
    .action(() => {
})
    .parse(process.argv);
if (!commander['url']) {
    commander.help();
}
fs.stat(commander['pages'], (err, stats) => {
    if (!stats.isFile()) {
        console.error(`Pages JSON file not found on path: ${commander['pages']}`);
        // commander.exit(1);
        return 0;
    }
    else {
        fs.readFile(commander['pages'], 'utf8', (err, data) => {
            let sitemap = new sitemap_1.Sitemap(commander['url'], commander['save'], commander['global'], DEFAULT_LOCALES);
            let confPages = JSON.parse(data);
            let pages = sitemap.process(confPages);
            // TODO Вынести в callback
            http.get('http://app.essaypro.com/api/catalog?limit=50', (res) => {
                res.setEncoding('utf8');
                let responseData = '';
                res.on('data', (chunk) => {
                    responseData += chunk;
                });
                res.on('end', () => {
                    let jsonResponse = JSON.parse(responseData);
                    let writers = jsonResponse['writers'];
                    writers.forEach((writer) => {
                        let page = {
                            url: `essay-writer.html?id=${writer.id}`,
                            locales: DEFAULT_LOCALES
                        };
                        pages.push(page);
                    });
                    let xml = sitemap.generate(pages);
                    sitemap.save(xml, false);
                    // TODO Generate siteindex.xml
                    // commander.exit(0);
                });
            }).on('error', (e) => {
                console.log(`Got error: ${e.message}`);
            });
        });
    }
});
